<?php
	session_start();
	if(!isset($_SESSION["question"]))
	{
		$_SESSION["time_started"] = time();
		$_SESSION["question"] = 0;
		$_SESSION["score"] = 0;
		$_SESSION["username"] = "";
	}
	if(isset($_POST["username"]))
	{
		if(!hasNotTakenTestBefore($_POST["username"]))
		{
			echo("Error: you've taken this test before");
			session_destroy();
		}
		else if(!checkFile($_POST["username"], $_POST["password"]))
		{
			echo("Sorry, login wasn't successful. Either your username doesn't exist, or your password was incorrect");
			session_destroy();
		}
		else
		{
			welcomePage();
		}
	}
	else if($_SESSION["username"] == "")
	{
		loginPage();
	}
	else
	{
		displayQuestion($_SESSION["question"]);
	}
	function loginPage()
	{
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Login
					</h1>
					<form method = "post" action = $script>
						Username: <input type="text" name="username">
						<br><br>
						Password: <input type="password" name="password">
						<br><br>
						<input type="submit" value="Continue" name="continue">
					</form>
				</body>
			</html>
TOP;
	}
	function checkFile($username, $password)
	{
		$fin = fopen ("./passwd", "r");
		while(!feof($fin))
		{
			$val = trim(fgets($fin));
			$t_idx = strrpos($val, ":");
			if(!$t_idx)
				continue;
			$t_username = substr($val, 0, $t_idx);
			$t_password = substr($val, $t_idx + 1);
			if($username == $t_username)
			{
				fclose($fin);
				if($password == $t_password)
				{
					$_SESSION["username"] = $username;
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		fclose($fin);
		return false;
	}
	function hasNotTakenTestBefore($username)
	{
		$fin = fopen ("./results", "r");
		while(!feof($fin))
		{
			$val = trim(fgets($fin));
			$t_idx = strrpos($val, ":");
			if(!$t_idx)
				continue;
			$t_username = substr($val, 0, $t_idx);
			if($username == $t_username)
			{
				$_SESSION["username"] = $username;
				return false;
			}
		}
		fclose($fin);
		return true;

	}
	function welcomePage()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					You will be given 6 questions in this quiz.
					<form method = "post" action = $script>
						<br><br>
						<input type="submit" value="Continue" name="continue">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion($idx)
	{
		if(time() - $_SESSION["time_started"] > 900)
		{
			writeResultsToFile();
			displayScore();
			session_destroy();
		}
		if($idx == 1)
		{
			displayQuestion1();
		}
		if($idx == 2)
		{
			gradeQuestion1();
			displayQuestion2();
		}
		if($idx == 3)
		{
			gradeQuestion2();
			displayQuestion3();
		}
		if($idx == 4)
		{
			gradeQuestion3();
			displayQuestion4();
		}
		if($idx == 5)
		{
			gradeQuestion4();
			displayQuestion5();
		}
		if($idx == 6)
		{
			gradeQuestion5();
			displayQuestion6();
		}
		else if($idx == 7)
		{
			gradeQuestion6();
			writeResultsToFile();
			displayScore();
			session_destroy();
		}
	}
	function displayQuestion1()
	{
		$_SESSION["time_started"] = time();
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 1
					</h3>
					<form method="post" action=$script>
						According to Kepler, the orbit of the earth is a circle with the sun at the center.
						<br>
						<input type="radio" value="True" name="Question_1">
							True		
						<br>
						<input type="radio" value="False" name="Question_1">
							False
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion2()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 2
					</h3>
					<form method="post" action=$script>
						Ancient Astronomers did consider the heliocentric model of the solar system but rejected it because they could not detect parallax.
						<br>
						<input type="radio" value="True" name="Question_2">
							True		
						<br>
						<input type="radio" value="False" name="Question_2">
							False
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion3()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 3
					</h3>
					<form method="post" action=$script>
						The total amount of energy that a star emits is directly related to its
						<br>
						<input type="checkbox" name="Question_3[]" value="A">
							surface gravity and magnetic field
						<br>
						<input type="checkbox" name="Question_3[]" value="B">
							radius and temperature
						<br>
						<input type="checkbox" name="Question_3[]" value="C">
							pressure and volume
						<br>
						<input type="checkbox" name="Question_3[]" value="D">
							location and velocity
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion4()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 4
					</h3>
					<form method="post" action=$script>
						Stars that live the longest have
						<br>
						<input type="checkbox" name="Question_4[]" value="A">
							high mass
						<br>
						<input type="checkbox" name="Question_4[]" value="B">
							high temperature
						<br>
						<input type="checkbox" name="Question_4[]" value="C">
							lots of hydrogen
						<br>
						<input type="checkbox" name="Question_4[]" value="D">
							small mass
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion5()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 5
					</h3>
					<form method="post" action=$script>
						A collection of a hundred billion stars, gas, and dust is called a 
						<input type="text" name="Question_5">.
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function displayQuestion6()
	{
		$_SESSION["question"] += 1;
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Astronomy Quiz
					</title>
				</head>
				<body>
					<h1>
						Astronomy Quiz
					</h1>
					<h3>
						Question 6
					</h3>
					<form method="post" action=$script>
						The inverse of the Hubble's constant is a measure of the
						<input type="text" name="Question_6">
						of the universe.
						<br><br>
						<input type="submit" value="Submit">
					</form>
				</body>
			</html>
TOP;
	}
	function gradeQuestion1()
	{
		if(strcmp($_POST["Question_1"], "False") == 0)
			$_SESSION["score"] += 10;
	}
	function gradeQuestion2()
	{
		if(strcmp($_POST["Question_2"], "True") == 0)
			$_SESSION["score"] += 10;
	}
	function gradeQuestion3()
	{
		if(count($_POST["Question_3"]) == 1 && strcmp($_POST["Question_3"][0], "B") == 0)
				$_SESSION["score"] += 10;
	}
	function gradeQuestion4()
	{
		if(count($_POST["Question_4"]) == 1 && strcmp($_POST["Question_4"][0], "D") == 0)
				$_SESSION["score"] += 10;
	}
	function gradeQuestion5()
	{
		if(strcasecmp($_POST["Question_5"], "galaxy") == 0) 
			$_SESSION["score"] += 10;
	}
	function gradeQuestion6()
	{
		if(strcasecmp($_POST["Question_6"], "age") == 0) 
			$_SESSION["score"] += 10;
	}
	function displayScore()
	{
		print<<<TOP
		<html>
			<head>
				<title>
					Astronomy Quiz
				</title>
			</head>
			<body>
				<h1>
					Astronomy Quiz
				</h1>
				Your score is
TOP;
		print(' ' . $_SESSION["score"]);
		print<<<BOTTOM
			</body>
		</html>
BOTTOM;
	}
	function writeResultsToFile()
	{
		$val = $_SESSION["username"] . ":" . $_SESSION["score"] . "\n";
		file_put_contents("results", $val, FILE_APPEND);
	}
?>

<?php
	$article_idx = $_GET['article'];
	print<<<TOP
	<html lang="en">
		<head>
			<title>
				Login or Register
			</title>
			<meta charset="utf-8">
		</head>
		<body>
			It appears that you aren't logged in. Would you like to register or login?
			<br>
			<input type="button" value="Login" onclick="location.href='https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk14/login.php?article=$article_idx'">
			<br>
			<input type="button" value="Register" onclick="location.href='https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk14/register.php?article=$article_idx'">
		</body>
	</html>
TOP;
?>

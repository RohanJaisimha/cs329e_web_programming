<?php
	$script_name = $_SERVER['PHP_SELF'];
	if(!isset($_POST['user_name']))
		form();
	else
	{
		if(checkCredentials())
		{
			setcookie("logged_in", "true", time() + 120);
			$article_idx = $_GET['article'];
			header("location:https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk14/news.php?article=" . $article_idx);
		}
		else
		{
			echo("Registration wasn't successful. Either the username is already taken, or the passwords don't match");
		}
	}
	function form()
	{
		print<<<TOP
		<html>
		<head>
		<title> Registration </title>
		</head>
		<body>
			 <form method="post" action= "$script_name">
				User name:<br>
				<input type="text" name="user_name" id="user_name"><br>
				Password:<br>
				<input type="password" name="password" id="password">
				<br>
				Confirm password:<br>
				<input type="password" name="password_2" id="password_2">
				<br><br>
				<input type="submit" id="submitBtn" value="Register">
				<input type="reset" val="Clear">
			</form>
		</body>
		</html>
TOP;
	}
	function checkCredentials()
	{
		$username = $_POST['user_name'];
		$fin = fopen ("./passwd", "r");
		while(!feof($fin))
		{
			$val = trim(fgets($fin));
			$t_idx = strrpos($val, ":");
			$t_username = substr($val, 0, $t_idx);
			if($username == $t_username)
			{
				return false;
			}
		}
		fclose($fin);
		$password = $_POST['password'];
		$password_2 = $_POST['password_2'];
		if($password == $password_2)
		{
			$fout = fopen("./passwd", "a");
			fwrite($fout, $username . ":" . $password . "\n");
			fclose($fout);
			return true;
		}
		else
			return false;
	}
?>

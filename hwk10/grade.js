function grade()
{
	if(!allQuestionsHaveBeenAnswered())
	{
		alert("Please answer all questions");
		return;
	}
	let score = 0;
	score = score + ((Q1Correct()) ? 1 : 0);
	score = score + ((Q2Correct()) ? 1 : 0);
	score = score + ((Q3Correct()) ? 1 : 0);
	score = score + ((Q4Correct()) ? 1 : 0);
	score = score + ((Q5Correct()) ? 1 : 0);
	score = score + ((Q6Correct()) ? 1 : 0);
	alert("Your grade is " + score +" / 6");
}
function allQuestionsHaveBeenAnswered()
{
	return Q1Answered() && Q2Answered() && Q3Answered() && Q4Answered() && Q5Answered() && Q6Answered();
}
function Q1Answered()
{
	let Q1_Answers = document.getElementsByName("Q1");
	for(let i = 0; i < Q1_Answers.length; i += 1)
	{
		if(Q1_Answers[i].checked)
			return true;
	}
	return false;
}
function Q2Answered()
{
	let Q2_Answers = document.getElementsByName("Q2");
	for(let i = 0; i < Q2_Answers.length; i += 1)
	{
		if(Q2_Answers[i].checked)
			return true;
	}
	return false;
}
function Q3Answered()
{
	let Q3_Answers = document.getElementsByName("Q3");
	for(let i = 0; i < Q3_Answers.length; i += 1)
	{
		if(Q3_Answers[i].checked)
			return true;
	}
	return false;
}
function Q4Answered()
{
	let Q4_Answers = document.getElementsByName("Q4");
	for(let i = 0; i < Q4_Answers.length; i += 1)
	{
		if(Q4_Answers[i].checked)
			return true;
	}
	return false;
}
function Q5Answered()
{
	let Q5_Answer = document.getElementsByName("Q5")[0].value;
	return Q5_Answer !== "";
}
function Q6Answered()
{
	let Q6_Answer = document.getElementsByName("Q6")[0].value;
	return Q6_Answer !== "";
}
function Q1Correct()
{
	let Q1_Answers = document.getElementsByName("Q1");
	return Q1_Answers[1].checked;
}
function Q2Correct()
{
	let Q2_Answers = document.getElementsByName("Q2");
	return Q2_Answers[0].checked;
}
function Q3Correct()
{
	let Q3_Answers = document.getElementsByName("Q3");
	let num_checked = 0;
	for(let i = 0; i < Q3_Answers.length; i += 1)
	{
		if(Q3_Answers[i].checked)
			num_checked += 1;
	}
	return num_checked === 1 && Q3_Answers[1].checked;
}
function Q4Correct()
{
	let Q4_Answers = document.getElementsByName("Q4");
	let num_checked = 0;
	for(let i = 0; i < Q4_Answers.length; i += 1)
	{
		if(Q4_Answers[i].checked)
			num_checked += 1;
	}
	return num_checked === 1 && Q4_Answers[3].checked;
}
function Q5Correct()
{
	let Q5_Answer = document.getElementsByName("Q5")[0].value.toLowerCase();
	return Q5_Answer === "galaxy";
}
function Q6Correct()
{
	let Q6_Answer = document.getElementsByName("Q6")[0].value.toLowerCase();
	return Q6_Answer === "age";
}

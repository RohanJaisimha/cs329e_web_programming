<?php
	if (isset($_POST["confirm"]))
	{
		updateFile();
		signUpSheet();
	}
	else
	{
		signUpSheet();
	}
	function getSignUpSheet($f_name)
	{
		$arr = array();
		$fin = fopen("./".$f_name, "r");
		while(!feof($fin))
		{
			$line = rtrim(fgets($fin));
			$line = explode("-",$line);
			$arr[$line[0]] = $line[1];
		}
		unset($arr[""]);
		fclose($fin);
		return $arr;
	}
	function signUpSheet()
	{
		$script = $_SERVER['PHP_SELF'];
		$sign_up_sheet = getSignUpSheet("signup.txt");
		print <<<TOP
		<html>
			<head>
				<title>
					Sign Up Sheet
				</title>
				<meta charset="utf-8">
				<style>
					table, tr, td
					{
						border:1px solid black;
					}
				</style>
			</head>
			<body style="text-align:center;">
				<h1>
					Sign Up Sheet
				</h1>
				<form method="post" action="$script">
					<table style="margin:auto;">
						<thead>
							<tr>
								<td style="font-weight:900;text-align:center;text-decoration:underline;">
									Time
								</td>
								<td style="font-weight:900;text-align:center;text-decoration:underline;">	
									Name
								</td>
							</tr>
						</thead>
						<tbody>
TOP;
							$i = 1;
							foreach($sign_up_sheet as $time=>$name)
							{	
								$id_and_name_of_text_box = "name_box_".strval($i);
								$second_column_val = "";
								if(strcmp($name, "0") == 0)
								{
									$second_column_val = '<input type="text" id='.$id_and_name_of_text_box.' name='.$id_and_name_of_text_box.'>';
								}
								else
								{
									$second_column_val = $name;
								}	
								print <<<TABLE_BODY
								<tr>
									<td>
										$time
									</td>
									<td>
										$second_column_val
									</td>
								</tr>
TABLE_BODY;
								$i += 1;
							}
							print <<<BOTTOM
							<tr>
								<td colspan=2 style="text-align:center;">
									<input type="submit" name="confirm" value="Sign up">
								</td>
							</tr>								
						</tbody>
					</table>
				</form>
			</body>
		</html>
BOTTOM;
	}
	function writeToFile($f_name, $arr)
	{
		$fout = fopen($f_name, "w");
		foreach($arr as $key=>$val)
			fwrite($fout, $key."-".$val."\n");
		fclose($fout);
	}
	function updateFile()
	{
		$val = 0;
		$idx = 0;
		$sign_up_sheet = getSignUpSheet("signup.txt");
		for($i = 1; $i < 12; $i += 1)
		{
			if(strcmp($_POST["name_box_".strval($i)],"") != 0)
			{
				$val = $_POST["name_box_".strval($i)];
				$idx = $i - 1;
				break;
			}
		}
		$sign_up_sheet[array_keys($sign_up_sheet)[$idx]] = $val;
		writeToFile("signup.txt", $sign_up_sheet);
	}
?>

<?php
	if(!isset($_COOKIE["logged_in"]))
	{
		echo("You have't logged in");
		echo("<br><br><br><a href=\"./loginPage.php\">Go to login page</a>");
		exit();
	}
	if(isset($_POST["choice"]))
	{
		if($_POST["choice"] == "insert")
			header("location:https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk16/insertPage.php");
		else if($_POST["choice"] == "update")
			header("location:https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk16/updatePage.php");
		else if($_POST["choice"] == "delete")
			header("location:https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk16/deletePage.php");
		else if($_POST["choice"] == "view")
			header("location:https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk16/viewPage.php");
		else if($_POST["choice"] == "logout")
		{
			setcookie("logged_in", "", 1);
			echo("<h1>Thank you</h1>");
		}
	}
	else
	{
		displayMenu();
	}
	function displayMenu()
	{
		$script = $_SERVER["PHP_SELF"];
		print<<<TOP
			<html>
				<head>
					<title>
						Menu
					</title>
				</head>
				<body>
					<h3>
						Please select one of the following options
					</h3>
					<form action=$script method="post">
						<input type="radio" name="choice" value="insert">
							Insert Student Record
						<br>
						<input type="radio" name="choice" value="update">
							Update Student Record
						<br>
						<input type="radio" name="choice" value="delete">
							Delete Student Record
						<br>
						<input type="radio" name="choice" value="view">
							View Student Record
						<br>
						<input type="radio" name="choice" value="logout">
							Logout
						<br><br>
						<input type="submit" value="Submit">
				</body>
			</html>
TOP;
	}
?>

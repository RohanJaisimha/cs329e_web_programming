<?php
	if(!isset($_COOKIE["logged_in"]))
	{
		echo("You have't logged in");
		echo("<br><br><br><a href=\"./loginPage.php\">Go to login page</a>");
		exit();
	}
	if(isset($_POST["id"]))
	{
		queryDatabase();
	}
	else
	{
		displayForm();
	}
	function displayForm()
	{
		$script = $_SERVER["PHP_SELF"];
		print<<<TOP
			<html>
				<head>
					<title>
						Delete Student Record
					</title>
				</head>
				<body>
					<form action=$script method="post" onsubmit="return validateForm()"> 
						ID to be Deleted: <input type="text" name="id">
						<br><br>
						<input type="submit" value="Submit">
						<input type="reset" value="Reset">
					</form>
					<div id="error_message" style="display:none">
					</div>
				</body>
				<script>
					function validateForm()
					{
						let ID = document.getElementsByName("id")[0];
						if(ID.value !== "")
						{
							return true;
						}
						else
						{
							let elem = document.getElementById("error_message");
							elem.innerHTML = ("<br><br><br><strong>ID field should be non-empty</strong>");
							elem.style.display = "block";
							return false;
						}
					}
				</script>
			</html>
TOP;
	}
	function queryDatabase()
	{
		print("<html><head><title>Result</title></head><body>");
		$id = $_POST["id"];
		$query = "DELETE FROM STUDENTS WHERE ID=" . $id . ";";
		$mysqli = mysqli_connect("fall-2019.cs.utexas.edu", "cs329e_mitra_jaisimha", "rump9Easy&suffer", "cs329e_mitra_jaisimha", "3306");
		mysqli_query($mysqli, $query);
		print("Successfully deleted.");
		mysqli_close($mysqli);
		
		$script = $_SERVER["PHP_SELF"];
		print("<br><br><a href=\"" . $script . "\">Go back to the form</a>");
		print("<br><a href=\"./homePage.php\">Go to the home page</a>");
		print("</body></html>");
	}
?>

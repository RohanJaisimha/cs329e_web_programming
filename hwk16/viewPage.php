<?php
	if(!isset($_COOKIE["logged_in"]))
	{
		echo("You have't logged in");
		echo("<br><br><br><a href=\"./loginPage.php\">Go to login page</a>");
		exit();
	}
	if(isset($_POST["id"]))
	{
		queryDatabase();
	}
	else
	{
		displayForm();
	}
	function displayForm()
	{
		$script = $_SERVER["PHP_SELF"];
		print<<<TOP
			<html>
				<head>
					<title>
						View Student Record
					</title>
				</head>
				<body>
					<form action=$script method="post" onsubmit="return validateForm()"> 
						ID: <input type="text" name="id">
						<br><br>
						Last Name: <input type="text" name="lastname">
						<br><br>
						First Name: <input type="text" name="firstname">
						<br><br>
						<input type="checkbox" name="all">
							View All Student Records
						<br><br>
						<input type="submit" value="Submit">
						<input type="reset" value="Reset">
					</form>
					<div id="error_message" style="display:none">
					</div>
				</body>
				<script>
					function validateForm()
					{
						let ID = document.getElementsByName("id")[0];
						let FN = document.getElementsByName("firstname")[0];
						let LN = document.getElementsByName("lastname")[0];
						let CB = document.getElementsByName("all")[0];
						if(ID.value !== "" || FN.value !== "" || LN.value !== "" || CB.checked)
						{
							return true;
						}
						else
						{
							let elem = document.getElementById("error_message");
							elem.innerHTML = ("<br><br><br><strong>Atleast one field should be non-empty</strong>");
							elem.style.display = "block";
							return false;
						}
					}
				</script>
			</html>
TOP;
	}
	function queryDatabase()
	{
		print("<html><head><title>Result</title></head><body>");
		$id = $_POST["id"];
		$firstname = $_POST["firstname"];
		$lastname = $_POST["lastname"];
		$query = "";
		if($_POST["all"] == "on")
		{
			$query = "SELECT * FROM STUDENTS ORDER BY LAST, FIRST;";
		}
		else if($id != "")
		{
			$query = "SELECT * FROM STUDENTS WHERE ID=" . $id . ";";
		}
		else if($firstname != "" && $lastname != "")
		{
			$query = "SELECT * FROM STUDENTS WHERE FIRST LIKE \"" . $firstname . "\" AND LAST LIKE \"" . $lastname . "\";";
		}
		else if($firstname != "")
		{
			$query = "SELECT * FROM STUDENTS WHERE FIRST LIKE \"" . $firstname . "\";";
		}
		else
		{
			$query = "SELECT * FROM STUDENTS WHERE LAST LIKE \"" . $lastname . "\";";
		}
		$mysqli = mysqli_connect("fall-2019.cs.utexas.edu", "cs329e_mitra_jaisimha", "rump9Easy&suffer", "cs329e_mitra_jaisimha", "3306");
		$result = mysqli_query($mysqli, $query);
		if($result->num_rows == 0)
		{
			print("Sorry, no records exist with those constraints");
		}
		else
		{
			displayAsTable($result);
		}
		mysqli_close($mysqli);
		
		$script = $_SERVER["PHP_SELF"];
		print("<br><br><a href=\"" . $script . "\">Go back to the form</a>");
		print("<br><a href=\"./homePage.php\">Go to the home page</a>");
		print("</body></html>");
	}
	function displayAsTable($result)
	{
		$s = "<table border=\"1\"><thead><tr><th>ID</th><th>LAST</th><th>FIRST</th><th>MAJOR</th><th>GPA</th></tr></thead><tbody>";
		while($row = $result->fetch_row())
		{
			$t = "<tr>";
	
			for($i = 0; $i < 5; $i += 1)
			{
				$t = $t . "<td>" . $row[$i] . "</td>";
			}
			$s = $s . $t . "</tr>";
		}
		$s = $s . "</body></table>";
		print($s);
	}
?>

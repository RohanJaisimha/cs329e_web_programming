<?php
	if(isset($_POST["username"]))
	{
		if(validateCredentials())
		{
			setCookie("logged_in", "true", time() + 24 * 3600);
			sleep(2);
			header("location:./homePage.php");
		}
		else
			echo("Login Failed");
	}
	else
	{
		displayLoginPage();
	}
	function displayLoginPage()
	{
		$script = $_SERVER['PHP_SELF'];
		print<<<TOP
			<html>
				<head>
					<title>
						Login
					</title>
				</head>
				<body>
					<h3>
						Student Records Login
					</h3>
					<form action=$script method="post">
						Username:
						<input type="text" name="username">
						<br><br>
						Password:
						<input type="password" name="password">
						<br><br>
						<input type="submit" value="Login">
						<input type="reset" value="Reset">
					</form>
				</body>
			</html>
				
TOP;
	}
	function validateCredentials()
	{
		$username = $_POST["username"];
		$password = $_POST["password"];	
		$fin = fopen ("./dbase/passwd", "r");
		while(!feof($fin))
		{
			$val = trim(fgets($fin));
			$t_idx = strrpos($val, ":");
			if(!$t_idx)
				continue;
			$t_username = substr($val, 0, $t_idx);
			$t_password = substr($val, $t_idx + 1);
			if($username == $t_username)
			{
				fclose($fin);
				return ($password == $t_password);
			}
		}
		fclose($fin);
		return false;
	}
?>

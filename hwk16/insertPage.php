<?php
	if(!isset($_COOKIE["logged_in"]))
	{
		echo("You have't logged in");
		echo("<br><br><br><a href=\"https://fall-2019.cs.utexas.edu/cs329e-mitra/jaisimha/hwk16/loginPage.php\">Go to login page</a>");
		exit();
	}
	if(isset($_POST["id"]))
	{
		queryDatabase();
	}
	else
	{
		displayForm();
	}
	function displayForm()
	{
		$script = $_SERVER["PHP_SELF"];
		print<<<TOP
			<html>
				<head>
					<title>
						Insert Student Record
					</title>
				</head>
				<body>
					<form action=$script method="post" onsubmit="return validateForm()"> 
						ID: <input type="text" name="id">
						<br><br>
						Last name: <input type="text" name="lastname">
						<br><br>
						First Name: <input type="text" name="firstname">
						<br><br>
						Major: <input type="text" name="major">
						<br><br>
						GPA: <input type="text" name="gpa">
						<br><br>
						<input type="submit" value="Submit">
						<input type="reset" value="Reset">
					</form>
					<div id="error_message" style="display:none">
					</div>
				</body>
				<script>
					function validateForm()
					{
						let ID = document.getElementsByName("id")[0];
						let FN = document.getElementsByName("firstname")[0];
						let LN = document.getElementsByName("lastname")[0];
						let M = document.getElementsByName("major")[0];
						let GPA = document.getElementsByName("gpa")[0];
						if(ID.value !== "" && FN.value !== "" && LN.value !== "" && M !== "" && GPA !== "")
						{
							return true;
						}
						else
						{
							let elem = document.getElementById("error_message");
							elem.innerHTML = ("<br><br><br><strong>All fields should be non-empty</strong>");
							elem.style.display = "block";
							return false;
						}
					}
				</script>
			</html>
TOP;
	}
	function queryDatabase()
	{
		print("<html><head><title>Result</title></head><body>");
		$id = $_POST["id"];
		$firstname = $_POST["firstname"];
		$lastname = $_POST["lastname"];
		$major = $_POST["major"];
		$gpa = $_POST["gpa"];
		$query = "INSERT INTO STUDENTS VALUES(" . $id . ", \"" . $lastname . "\", \"". $firstname . "\", \"" . $major . "\", " . $gpa . ");";
		$mysqli = mysqli_connect("fall-2019.cs.utexas.edu", "cs329e_mitra_jaisimha", "rump9Easy&suffer", "cs329e_mitra_jaisimha", "3306");
		if(!mysqli_query($mysqli, $query))
		{
			print("Error: insertion has failed. The ID should be unique");
		}
		else
		{
			print("Successfully inserted.");
		}
		mysqli_close($mysqli);
		
		$script = $_SERVER["PHP_SELF"];
		print("<br><br><a href=\"" . $script . "\">Go back to the form</a>");
		print("<br><a href=\"./homePage.php\">Go to the home page</a>");
		print("</body></html>");
	}
?>
